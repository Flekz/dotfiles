if _command_exists hx; then
  export EDITOR="hx"
  export VISUAL="hx"
  export SUDO_EDITOR="hx"
fi
if _command_exists bat; then
  export PAGER="bat"
fi
export HISTSIZE=10000 # Maximum events for internal history
export SAVEHIST=10000 # Maximum events in history file
