if _command_exists sk; then
    for module in "$HOME"/.zshrc.d/skim/*
    do
      . "${module}"
    done

    # Use the fzf tab completion plugin with skim
    source "$HOME/.zshrc.d/fzf/fzf-tab/fzf-tab.plugin.zsh"
    zstyle ':fzf-tab:*' fzf-command sk
    zstyle ':fzf-tab:complete:*options' sk --preview "preview.sh {}"
    function sk_lazy() {
        zvm_bindkey viins '^T' skim-file-widget
    }
fi
if ! _command_exists sk; then
    function sk_lazy() {}
fi


# History database
if _command_exists atuin; then
    eval "$(atuin init zsh)"
    function zvm_after_init() {
        zvm_bindkey viins '^R' _atuin_search_widget
        zvm_bindkey viins '^[[A' _atuin_up_search_widget
        zvm_bindkey viins '^[OA' _atuin_up_search_widget
        # Needs to be sourced at the end
        source "$HOME/.zshrc.d/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh"
    }
fi
if ! _command_exists atuin; then
    function zvm_after_init() {
        # Needs to be sourced at the end
        source "$HOME/.zshrc.d/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh"
    }
fi


source "$HOME/.zshrc.d/zsh-vi-mode/zsh-vi-mode.plugin.zsh"
function my_zvm_after_lazy_keybindings() {
    zvm_bindkey vicmd 'gh' beginning-of-line
    zvm_bindkey vicmd 'gl' end-of-line
}

zvm_after_lazy_keybindings_commands+=(my_zvm_after_lazy_keybindings)
# zvm_after_init_commands+=(sk_lazy)
zvm_after_init_commands+=(zvm_after_init)
