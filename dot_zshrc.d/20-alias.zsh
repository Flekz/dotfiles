## Functions for aliases
_cheat() {
    if [ -z "${1}" ]; then
        curl -fLm 7 "https://cheat.sh" | bat
    else
        curl -fLm 7 "https://cheat.sh/${@}" | bat
    fi
}
## Cd up dirs
## Usage: cd.. 10 or cd.. dir_name
cd_up() {
    case $1 in
        *[!(0-9)]*) # If not a number, find folder matching string
            cd $( pwd | sed -r "s|(.*/$1[^/]*/).*|\1|" ) # cd
            ;; # if folder not found, don't cd
        *)
            cd $(printf "%0.0s../" $(seq 1 $1)); # If a number, cd x folders  upwards
            ;;
    esac
}

## Aliases
if _command_exists eza; then
  alias ls='eza --icons -a --group-directories-first'
fi
if _command_exists zoxide; then
  alias cd=z # zoxide
fi
alias 'cd..'=cd_up

alias cheat="_cheat"
alias skcd='cd "$(sk -m)"'
