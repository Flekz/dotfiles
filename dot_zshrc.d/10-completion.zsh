zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]}' # Case insensitive, UNLESS usage of upper case
zstyle ':completion:*' menu select
zstyle ':completion:*kill:*' command 'procs'
autoload -U compinit; compinit

source "$HOME/.zshrc.d/zsh-autosuggestions/zsh-autosuggestions.zsh"
